import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const routes = [
  {
    path: "/",
    children: [
      {
        path: "",
        name: "home",
        component: HomeView,
      },
      {
        path: "survey/:id/elements",
        name: "elements",
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/ElementsView.vue"),
      },
      {
        path: "survey/:id/elements/create",
        name: "elementCreate",
        component: () =>
          import(
            /* webpackChunkName: "about" */ "../views/ElementCreateView.vue"
          ),
      },
    ],
  },
  {
    path: "/survey-select",
    children: [
      {
        path: "",
        name: "surveySelect",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/SurveySelect.vue"),
      },
      {
        path: "fill/:id",
        name: "surveyFill",
        component: () =>
          import(/* webpackChunkName: "fill" */ "../views/SurveyFill.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
